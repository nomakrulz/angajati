package AngajatiApp.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    private Employee e1;
    private EmployeeMock employeeMock;
    private List<Employee> employeeList;

    @Before
    public void setUp() {
        e1 = new Employee();
        e1.setId(3);
        e1.setFirstName("Felix");
        e1.setLastName("Negoita");
        e1.setCnp("1840523124948");
        e1.setSalary(3400d);
        e1.setFunction(DidacticFunction.TEACHER);

        employeeMock = new EmployeeMock();
        employeeList = new ArrayList<>();
    }

    @After
    public void tearDown() {
        e1 = null;
        employeeMock = null;
    }

    @Test(expected = NullPointerException.class)
    public void testModifyEmployeeFunction1() {
        List <Employee> initialList = employeeMock.getEmployeeList();
        e1 = null;
        employeeList.add(e1);
        employeeMock.modifyEmployeeFunction(employeeMock.findEmployeeById(e1.getId()), DidacticFunction.CONFERENTIAR);
        List <Employee> modifiedList = employeeMock.getEmployeeList();
        assertEquals(0, initialList.equals(modifiedList));
    }

    @Test
    public void testModifyEmployeeFunction2() {
        employeeMock.modifyEmployeeFunction(employeeMock.findEmployeeById(e1.getId()), DidacticFunction.CONFERENTIAR);
        assertEquals(DidacticFunction.CONFERENTIAR, employeeMock.findEmployeeById(e1.getId()).getFunction());
    }



    @Test
    public void TC1() {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Felix");
        employee.setLastName("Negoita");
        employee.setCnp("1234567890123");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(2000.00);

        int nr_angajati = employeeMock.getEmployeeList().size();
        employeeMock.addEmployee(employee);
        assertEquals(nr_angajati+1, employeeMock.getEmployeeList().size());
    }


    @Test()
    public void TC2() {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Alexandru");
        employee.setLastName("Mucenic");
        employee.setCnp("1234567890123");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(-1.00);

        assertFalse(employeeMock.addEmployee(employee));
    }


    @Test()
    public void TC3() {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Felix");
        employee.setLastName("Negoita");
        employee.setCnp("123456789@#");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(2000.00);

        assertFalse(employeeMock.addEmployee(employee));
    }

    @Test()
    public void TC4() {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Alexandru");
        employee.setLastName("Mucenic");
        employee.setCnp("1234567890123");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(0.00);

        assertFalse(employeeMock.addEmployee(employee));
    }
}